Feuerwehrsport-Statistik-Ergebnisse
==============================
Dies sind die Ergebnisse der Feuerwehrsport-Statistiken. Von diesen PDF-Dateien sind die [Daten](https://github.com/georf/Feuerwehrsport-Statistik-Daten) extrahiert worden. Sie stehen allen Leuten zur Verfügung. Sie werden bei Änderungen aktualisiert.

Die Daten stammen von der Webseite:
http://www.feuerwehrsport-statistik.de
